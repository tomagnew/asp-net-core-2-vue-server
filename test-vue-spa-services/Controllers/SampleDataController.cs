using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.IO;
using Microsoft.Extensions.FileProviders;

namespace test_vue_spa_services.Controllers
{
    [Route("api/[controller]")]
    public class SampleDataController : Controller
    {
        IFileProvider _fileProvider;
        //public SampleDataController(IFileProvider provider)
        //{
        //    _fileProvider = provider;
        //}
        private static string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        //[HttpGet("[action]")]
        [HttpGet]
        public IEnumerable<MyUser> getUsers()
        {
            var users = JsonConvert.DeserializeObject<IEnumerable<MyUser>>(System.IO.File.ReadAllText("ClientApp/src/data/users.json"));
            return users;
        }

        public class MyUser
        {
            public int id { get; set; }
            public string name { get; set; }
            public string email { get; set; }
            public string phone { get; set; }
        }
    }
}
